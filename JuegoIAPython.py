import pygame
import random
import threading
# Define los colores que se utilizarán
WHITE = (255, 255, 255)
BLACK = (0, 0, 0)
RED = (255, 0, 0)
YELLOW = (255, 255, 0)

# Inicializa el juego
pygame.init()

# Define las dimensiones de la pantalla
SCREEN_WIDTH = 800
SCREEN_HEIGHT = 600
screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Juego de recolección de monedas")

# Define las clases para el jugador, las monedas y el enemigo
class Player(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([50, 50])
        self.image.fill(YELLOW)
        self.rect = self.image.get_rect()

    def update(self):
        # Mueve el jugador con las teclas de flecha
        keys = pygame.key.get_pressed()
        if keys[pygame.K_LEFT]:
            self.rect.x -= 5
        elif keys[pygame.K_RIGHT]:
            self.rect.x += 5
        if keys[pygame.K_UP]:
            self.rect.y -= 5
        elif keys[pygame.K_DOWN]:
            self.rect.y += 5

        # Detecta si el jugador colisiona con una moneda
        hit_list = pygame.sprite.spritecollide(self, coin_list, True)
        
        

        for hit in hit_list:
            coin_list.remove(hit)
        #for hit in hit_list:
            #coin_sound.play()

class Coin(pygame.sprite.Sprite):
    def __init__(self):
        super().__init__()
        self.image = pygame.Surface([30, 30])
        self.image.fill(WHITE)
        self.rect = self.image.get_rect()
        self.rect.x = random.randrange(SCREEN_WIDTH - self.rect.width)
        self.rect.y = random.randrange(SCREEN_HEIGHT - self.rect.height)

class Enemy(pygame.sprite.Sprite):
    collide = False
    def __init__(self, player, obstacles):
        super().__init__()
        self.image = pygame.Surface([50, 50])
        self.image.fill(RED)
        self.rect = self.image.get_rect()
        self.player = player
        self.obstacles = obstacles # lista de objetos obstáculos
        #Mando a el enemigo a una ubicacion 
        self.rect.x = random.randrange(SCREEN_WIDTH - self.rect.width)
        self.rect.y = random.randrange(SCREEN_HEIGHT - self.rect.height)


    def update(self):
        # Calcula la dirección hacia el jugador
        dx = self.player.rect.x - self.rect.x
        dy = self.player.rect.y - self.rect.y
        dist = (dx**2 + dy**2)**0.5
        if dist > 0:
            dx = dx / dist
            dy = dy / dist
            
            # Comprueba si hay un obstáculo en la trayectoria
            hit = self.rect.collidelist(self.obstacles)
            if hit != -1:
                # Si hay un obstáculo, calcula una nueva dirección
                obstacle = self.obstacles[hit]
                ox, oy = obstacle.center
                #new_dx, new_dy = self._avoid_obstacle(ox, oy)
                self._avoid_obstacle(ox, oy)
                #dx, dy = new_dx, new_dy
                self.collide = True
                return
            self.collide = False
            # Mueve el enemigo
            if self.collide == False:
                self.rect.x += dx * 3
                self.rect.y += dy * 3

        hit_list = pygame.sprite.spritecollide(self, player_list ,True)

            
    def _avoid_obstacle(self, ox, oy):
        # Calcula una nueva dirección para evitar el obstáculo

        if self.rect.y - player.rect.y < 0:
            self.rect.x -=  2
        if self.rect.y - player.rect.y > 0 :
            self.rect.x +=  2
        if self.rect.y - player.rect.y == 0:
            self.rect.x -=  2  
            
        if self.rect.x - player.rect.x < 0:
            self.rect.y +=  2
        if self.rect.x- player.rect.x > 0 :
            self.rect.y -=  2
        if self.rect.x - player.rect.x == 0:
            self.rect.y +=  2

            
    

# Crea las listas para los sprites y las monedas
all_sprites_list = pygame.sprite.Group()
coin_list = pygame.sprite.Group()

conlistRect = []
# Crea el jugador y el enemigo
player = Player()
#agrego a el player en una lista
player_list = pygame.sprite.Group()
all_sprites_list.add(player)

player_list.add(player)
for i in range(10):
    coin = Coin()
    coin_list.add(coin)
    conlistRect.append(coin.rect)
    all_sprites_list.add(coin)
conlistRect.append(player.rect)
#creo a el enemigo
enemy = Enemy(player,conlistRect)
all_sprites_list.add(enemy)



# Crea algunas monedas


# Carga los sonidos
#coin_sound = pygame.mixer.Sound("coin.wav")

# Define el bucle principal del juego
done = False
clock = pygame.time.Clock()

while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done=True
    # Actualiza los sprites
    all_sprites_list.update()

    # Dibuja los sprites en la pantalla
    screen.fill(BLACK)
    all_sprites_list.draw(screen)
    
    conlistRect.clear()
    for hit in coin_list:
        conlistRect.append(hit.rect)

    # Actualiza la pantalla
    pygame.display.flip()

    # Limita el bucle principal a 60 fotogramas por segundo
    clock.tick(60)
pygame.quit()