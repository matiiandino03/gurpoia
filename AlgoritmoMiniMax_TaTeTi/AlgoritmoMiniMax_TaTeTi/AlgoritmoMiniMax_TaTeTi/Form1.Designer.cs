﻿
namespace AlgoritmoMiniMax_TaTeTi
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.c1f1 = new System.Windows.Forms.Button();
            this.c2f1 = new System.Windows.Forms.Button();
            this.c3f1 = new System.Windows.Forms.Button();
            this.c1f2 = new System.Windows.Forms.Button();
            this.c2f2 = new System.Windows.Forms.Button();
            this.c3f2 = new System.Windows.Forms.Button();
            this.c1f3 = new System.Windows.Forms.Button();
            this.c2f3 = new System.Windows.Forms.Button();
            this.c3f3 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.pIA = new System.Windows.Forms.Label();
            this.pJugador = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // c1f1
            // 
            this.c1f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1f1.Location = new System.Drawing.Point(37, 27);
            this.c1f1.Name = "c1f1";
            this.c1f1.Size = new System.Drawing.Size(91, 84);
            this.c1f1.TabIndex = 0;
            this.c1f1.UseVisualStyleBackColor = true;
            this.c1f1.Click += new System.EventHandler(this.c1f1_Click);
            // 
            // c2f1
            // 
            this.c2f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2f1.Location = new System.Drawing.Point(134, 27);
            this.c2f1.Name = "c2f1";
            this.c2f1.Size = new System.Drawing.Size(91, 84);
            this.c2f1.TabIndex = 1;
            this.c2f1.UseVisualStyleBackColor = true;
            // 
            // c3f1
            // 
            this.c3f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3f1.Location = new System.Drawing.Point(231, 27);
            this.c3f1.Name = "c3f1";
            this.c3f1.Size = new System.Drawing.Size(91, 84);
            this.c3f1.TabIndex = 2;
            this.c3f1.UseVisualStyleBackColor = true;
            // 
            // c1f2
            // 
            this.c1f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1f2.Location = new System.Drawing.Point(37, 117);
            this.c1f2.Name = "c1f2";
            this.c1f2.Size = new System.Drawing.Size(91, 84);
            this.c1f2.TabIndex = 3;
            this.c1f2.UseVisualStyleBackColor = true;
            // 
            // c2f2
            // 
            this.c2f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2f2.Location = new System.Drawing.Point(134, 117);
            this.c2f2.Name = "c2f2";
            this.c2f2.Size = new System.Drawing.Size(91, 84);
            this.c2f2.TabIndex = 4;
            this.c2f2.UseVisualStyleBackColor = true;
            // 
            // c3f2
            // 
            this.c3f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3f2.Location = new System.Drawing.Point(231, 117);
            this.c3f2.Name = "c3f2";
            this.c3f2.Size = new System.Drawing.Size(91, 84);
            this.c3f2.TabIndex = 5;
            this.c3f2.UseVisualStyleBackColor = true;
            // 
            // c1f3
            // 
            this.c1f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1f3.Location = new System.Drawing.Point(37, 207);
            this.c1f3.Name = "c1f3";
            this.c1f3.Size = new System.Drawing.Size(91, 84);
            this.c1f3.TabIndex = 6;
            this.c1f3.UseVisualStyleBackColor = true;
            // 
            // c2f3
            // 
            this.c2f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2f3.Location = new System.Drawing.Point(134, 207);
            this.c2f3.Name = "c2f3";
            this.c2f3.Size = new System.Drawing.Size(91, 84);
            this.c2f3.TabIndex = 7;
            this.c2f3.UseVisualStyleBackColor = true;
            // 
            // c3f3
            // 
            this.c3f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 50.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3f3.Location = new System.Drawing.Point(231, 207);
            this.c3f3.Name = "c3f3";
            this.c3f3.Size = new System.Drawing.Size(91, 84);
            this.c3f3.TabIndex = 8;
            this.c3f3.UseVisualStyleBackColor = true;
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(104, 311);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(146, 35);
            this.button1.TabIndex = 9;
            this.button1.Text = "button1";
            this.button1.UseVisualStyleBackColor = true;
            this.button1.Click += new System.EventHandler(this.button1_Click);
            // 
            // pIA
            // 
            this.pIA.AutoSize = true;
            this.pIA.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pIA.Location = new System.Drawing.Point(328, 61);
            this.pIA.Name = "pIA";
            this.pIA.Size = new System.Drawing.Size(29, 20);
            this.pIA.TabIndex = 10;
            this.pIA.Text = "IA:";
            // 
            // pJugador
            // 
            this.pJugador.AutoSize = true;
            this.pJugador.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.pJugador.Location = new System.Drawing.Point(328, 86);
            this.pJugador.Name = "pJugador";
            this.pJugador.Size = new System.Drawing.Size(71, 20);
            this.pJugador.TabIndex = 11;
            this.pJugador.Text = "Jugador:";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(328, 27);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(120, 25);
            this.label3.TabIndex = 12;
            this.label3.Text = "Puntuacion";
            this.label3.Click += new System.EventHandler(this.label3_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(456, 358);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.pJugador);
            this.Controls.Add(this.pIA);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.c3f3);
            this.Controls.Add(this.c2f3);
            this.Controls.Add(this.c1f3);
            this.Controls.Add(this.c3f2);
            this.Controls.Add(this.c2f2);
            this.Controls.Add(this.c1f2);
            this.Controls.Add(this.c3f1);
            this.Controls.Add(this.c2f1);
            this.Controls.Add(this.c1f1);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button c1f1;
        private System.Windows.Forms.Button c2f1;
        private System.Windows.Forms.Button c3f1;
        private System.Windows.Forms.Button c1f2;
        private System.Windows.Forms.Button c2f2;
        private System.Windows.Forms.Button c3f2;
        private System.Windows.Forms.Button c1f3;
        private System.Windows.Forms.Button c2f3;
        private System.Windows.Forms.Button c3f3;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.Label pIA;
        private System.Windows.Forms.Label pJugador;
        private System.Windows.Forms.Label label3;
    }
}

