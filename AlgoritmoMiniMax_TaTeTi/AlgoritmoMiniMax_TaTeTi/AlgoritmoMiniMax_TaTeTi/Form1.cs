﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace AlgoritmoMiniMax_TaTeTi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        public int[,] Tablero = new int[3, 3];
        public int[] IAUltimoMov = new int[3];
        public int Ganador = -1;
        bool inicioJugador = true;

        int puntIA { get; set; }
        int puntPJ { get; set; }

        private void Form1_Load(object sender, EventArgs e)
        {
            #region Asiganciones
            c1f1.Click += ClickBoton;
            c1f2.Click += ClickBoton;
            c1f3.Click += ClickBoton;
            c2f1.Click += ClickBoton;
            c2f2.Click += ClickBoton;
            c2f3.Click += ClickBoton;
            c3f1.Click += ClickBoton;
            c3f2.Click += ClickBoton;
            c3f3.Click += ClickBoton;
            #endregion

            IniciarNuevaPartida();
            button1.Text = "Nueva Partida";

            pIA.Text = "IA :" + puntIA.ToString();
            pJugador.Text = "Jugador :" + puntPJ.ToString();
        }

        public void Actualizar()
        {
            if(Tablero[0, 0] == 1)
            {
                c1f1.Text = "O";
                c1f1.Enabled = false;
            }
            if (Tablero[0, 1] == 1)
            {
                c2f1.Text = "O";
                c2f1.Enabled = false;
            }
            if (Tablero[0, 2] == 1)
            {
                c3f1.Text = "O";
                c3f1.Enabled = false;
            }
            if (Tablero[1, 0] == 1)
            {
                c1f2.Text = "O";
                c1f2.Enabled = false;
            }
            if (Tablero[1, 1] == 1)
            {
                c2f2.Text = "O";
                c2f2.Enabled = false;
            }
            if (Tablero[1, 2] == 1)
            {
                c3f2.Text = "O";
                c3f2.Enabled = false;
            }
            if (Tablero[2, 0] == 1)
            {
                c1f3.Text = "O";
                c1f3.Enabled = false;
            }
            if (Tablero[2, 1] == 1)
            {
                c2f3.Text = "O";
                c2f3.Enabled = false;
            }
            if (Tablero[2, 2] == 1)
            {
                c3f3.Text = "O";
                c3f3.Enabled = false;
            }
            pIA.Text = "IA :" + puntIA.ToString();
            pJugador.Text = "Jugador :" + puntPJ.ToString();
        }
        public void ClickBoton(Object sender, EventArgs e)
        {
            ((Button)sender).Text = "X";
            if(((Button)sender).Name == "c1f1")
            {
                JugarPosicion(0, 0);
            }
            if (((Button)sender).Name == "c1f2")
            {
                JugarPosicion(1, 0);
            }
            if (((Button)sender).Name == "c1f3")
            {
                JugarPosicion(2, 0);
            }
            if (((Button)sender).Name == "c2f1")
            {
                JugarPosicion(0, 1);
            }
            if (((Button)sender).Name == "c2f2")
            {
                JugarPosicion(1, 1);
            }
            if (((Button)sender).Name == "c2f3")
            {
                JugarPosicion(2, 1);
            }
            if (((Button)sender).Name == "c3f1")
            {
                JugarPosicion(0, 2);
            }
            if (((Button)sender).Name == "c3f2")
            {
                JugarPosicion(1, 2);
            }
            if (((Button)sender).Name == "c3f3")
            {
                JugarPosicion(2, 2);
            }
            ((Button)sender).Enabled = false;

            if (CheckWinner() == 1)
            {
                MessageBox.Show("Gano la IA");
                puntIA++;
            }
            if (CheckWinner() == 0)
            {
                MessageBox.Show("Gano el jugador");
                puntPJ++;
            }
            if (CheckWinner() == -1 && TableroCompleto())
            {
                MessageBox.Show("EMPATE");
            }
            Actualizar();
        }


        public void IniciarNuevaPartida()
        {
            for (int i = 0; i < Tablero.GetLength(0); i++)
                for (int j = 0; j < Tablero.GetLength(1); j++)
                    Tablero[i, j] = -1;
            Ganador = -1;
        }

        public void JugarPosicion(int x, int y)
        {
            
             Tablero[x, y] = 0;
             Ganador = CheckWinner();
             JuegoIA();
            
        }

        public int CheckWinner()
        {
            int ganador = -1;

            if (Tablero[0, 0] != -1 && Tablero[0, 0] == Tablero[1, 1] && Tablero[0, 0] == Tablero[2, 2])
                ganador = Tablero[0, 0];

            if (Tablero[0, 2] != -1 && Tablero[0, 2] == Tablero[1, 1] && Tablero[0, 2] == Tablero[2, 0])
                ganador = Tablero[0, 2];

            for (int i = 0; i < Tablero.GetLength(0); i++)
            {
                if (Tablero[i, 0] != -1 && Tablero[i, 0] == Tablero[i, 1] && Tablero[i, 0] == Tablero[i, 2])
                    ganador = Tablero[i, 0];

                if (Tablero[0, i] != -1 && Tablero[0, i] == Tablero[1, i] && Tablero[0, i] == Tablero[2, i])
                    ganador = Tablero[0, i];
            }


            return ganador;
        }

        public bool TableroCompleto()
        {
            bool tableroCompleto = true;
            for (int i = 0; i < Tablero.GetLength(0); i++)
                for (int j = 0; j < Tablero.GetLength(1); j++)
                    if (Tablero[i, j] == -1)
                        tableroCompleto = false;


            return tableroCompleto;
        }

        public bool TerminoPartida()
        {
            bool termino = false;
            if (TableroCompleto() || CheckWinner() != -1)
                termino = true;

            return termino;

        }

        public void JuegoIA()
        {

            if (!TerminoPartida())
            {
                int f = 0;
                int c = 0;
                int valorJugada = int.MinValue;
                int auxiliar;

                for (int i = 0; i < Tablero.GetLength(0); i++)
                    for (int j = 0; j < Tablero.GetLength(1); j++)
                        if (Tablero[i, j] == -1)
                        {
                            Tablero[i, j] = 1;
                            auxiliar = Mini();
                            if (auxiliar > valorJugada)
                            {
                                valorJugada = auxiliar;
                                f = i;
                                c = j;
                            }

                            Tablero[i, j] = -1;
                        }
                Tablero[f, c] = 1;
                IAUltimoMov[0] = f;
                IAUltimoMov[1] = c;
            }

            Actualizar();
        }


        private int Max()
        {
            if (TerminoPartida())
            {
                if (CheckWinner() != -1)
                    return -1;
                else
                    return 0;
            }

            int valorJugada = int.MinValue;
            int auxiliar;
            for (int i = 0; i < Tablero.GetLength(0); i++)
                for (int j = 0; j < Tablero.GetLength(1); j++)
                    if (Tablero[i, j] == -1)
                    {
                        Tablero[i, j] = 1;
                        auxiliar = Mini();
                        if (auxiliar > valorJugada)
                            valorJugada = auxiliar;

                        Tablero[i, j] = -1;
                    }

            return valorJugada;
        }

        private int Mini()
        {
            if (TerminoPartida())
            {
                if (CheckWinner() != -1)
                    return 1;
                else
                    return 0;
            }

            int valorJugada = int.MaxValue;
            int auxiliar;
            for (int i = 0; i < Tablero.GetLength(0); i++)
                for (int j = 0; j < Tablero.GetLength(1); j++)
                    if (Tablero[i, j] == -1)
                    {
                        Tablero[i, j] = 0;
                        auxiliar = Max();
                        if (auxiliar < valorJugada)
                            valorJugada = auxiliar;

                        Tablero[i, j] = -1;
                    }

            return valorJugada;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            IniciarNuevaPartida();
            foreach(Control c in Controls)
            {
                c.Text = "";
                c.Enabled = true;
            }
            ((Button)sender).Text = "Nueva Partida";
            Ganador = -1;

            if(inicioJugador)
            {
                JuegoIA();
            }

            inicioJugador = !inicioJugador;

            pIA.Text = "IA :" + puntIA.ToString();
            pJugador.Text = "Jugador :" + puntPJ.ToString();
            label3.Text = "Puntuacion";
        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void c1f1_Click(object sender, EventArgs e)
        {

        }
    }
}
