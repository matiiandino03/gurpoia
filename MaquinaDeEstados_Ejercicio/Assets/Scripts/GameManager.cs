using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public static GameManager instance;

    public List<GameObject> NotInfected = new List<GameObject>();

    public GameObject txtGameOver;

    private void Awake()
    {
        if(instance == null)
        {
            instance = this;
        }
    }

    public void GameOver()
    {
        Time.timeScale = 0;
        txtGameOver.SetActive(true);
    }

}
