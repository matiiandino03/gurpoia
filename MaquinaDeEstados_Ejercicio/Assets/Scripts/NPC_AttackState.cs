using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NPC_AttackState : NPC_BaseState
{
    public NPC_AttackState(string name, NPC_Controller controller) : base(name, controller) { }

    public bool Infected = false;
    float x, z;
    public override void Iniciate(bool infected)
    {

        sm.GetComponent<Renderer>().material = ((NPC_Controller)sm).matAttacking;

        x = ((NPC_Controller)sm).transform.position.x;
        z = ((NPC_Controller)sm).transform.position.z;
    }

    public override void Refresh()
    {
        GameObject go = GetClosestNotInfected();
        if(distance < 3)
        {
            ((NPC_Controller)sm).navMeshAgent.SetDestination(new Vector3(go.transform.position.x, 1, go.transform.position.z));
            if (distance < 1)
            {
                if(go.GetComponent<NPC_Controller>() != null)
                {
                    go.GetComponent<NPC_Controller>().Infected();
                }
                else
                {
                    GameManager.instance.GameOver();
                    go.GetComponent<Renderer>().material = ((NPC_Controller)sm).matInfected;
                }
            }
        }        
        else
        {
            ((NPC_Controller)sm).SwitchToIdleState();
        }
    }

    public override void Finish()
    {
        sm.GetComponent<Renderer>().material = ((NPC_Controller)sm).matInfected;
    }

    public GameObject GetClosestNotInfected()
    {
        float distanceToClosest = int.MaxValue;
        GameObject gameObject = null;
        foreach (GameObject go in GameManager.instance.NotInfected)
        {
            if (Vector3.Distance(sm.gameObject.transform.position, go.transform.position) < distanceToClosest)
            {
                distanceToClosest = Vector3.Distance(sm.gameObject.transform.position, go.transform.position);
                gameObject = null;
                gameObject = go;
            }
        }
        distance = distanceToClosest;
        return gameObject;
    }
}
