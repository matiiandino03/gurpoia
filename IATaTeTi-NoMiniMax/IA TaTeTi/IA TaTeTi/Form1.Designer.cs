﻿
namespace IA_TaTeTi
{
    partial class Form1
    {
        /// <summary>
        /// Variable del diseñador necesaria.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Limpiar los recursos que se estén usando.
        /// </summary>
        /// <param name="disposing">true si los recursos administrados se deben desechar; false en caso contrario.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Código generado por el Diseñador de Windows Forms

        /// <summary>
        /// Método necesario para admitir el Diseñador. No se puede modificar
        /// el contenido de este método con el editor de código.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.txtTurno = new System.Windows.Forms.Label();
            this.textBox10 = new System.Windows.Forms.TextBox();
            this.textBox11 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.c1f1 = new Controls.Casillero();
            this.c2f1 = new Controls.Casillero();
            this.c3f1 = new Controls.Casillero();
            this.c1f2 = new Controls.Casillero();
            this.c2f2 = new Controls.Casillero();
            this.c3f2 = new Controls.Casillero();
            this.c1f3 = new Controls.Casillero();
            this.c2f3 = new Controls.Casillero();
            this.c3f3 = new Controls.Casillero();
            this.IAc3f3 = new Controls.Casillero();
            this.IAc2f3 = new Controls.Casillero();
            this.IAc1f3 = new Controls.Casillero();
            this.IAc3f2 = new Controls.Casillero();
            this.IAc2f2 = new Controls.Casillero();
            this.IAc1f2 = new Controls.Casillero();
            this.IAc3f1 = new Controls.Casillero();
            this.IAc2f1 = new Controls.Casillero();
            this.IAc1f1 = new Controls.Casillero();
            this.label1 = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // txtTurno
            // 
            this.txtTurno.AutoSize = true;
            this.txtTurno.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTurno.Location = new System.Drawing.Point(296, 44);
            this.txtTurno.Name = "txtTurno";
            this.txtTurno.Size = new System.Drawing.Size(70, 25);
            this.txtTurno.TabIndex = 9;
            this.txtTurno.Text = "Turno:";
            // 
            // textBox10
            // 
            this.textBox10.Enabled = false;
            this.textBox10.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox10.Location = new System.Drawing.Point(294, 139);
            this.textBox10.Name = "textBox10";
            this.textBox10.Size = new System.Drawing.Size(35, 53);
            this.textBox10.TabIndex = 10;
            this.textBox10.Text = "X";
            // 
            // textBox11
            // 
            this.textBox11.Enabled = false;
            this.textBox11.Font = new System.Drawing.Font("Microsoft Sans Serif", 30F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.textBox11.Location = new System.Drawing.Point(294, 198);
            this.textBox11.Name = "textBox11";
            this.textBox11.Size = new System.Drawing.Size(35, 53);
            this.textBox11.TabIndex = 11;
            this.textBox11.Text = "O";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(335, 159);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(31, 25);
            this.label2.TabIndex = 12;
            this.label2.Text = "IA";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(335, 218);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(84, 25);
            this.label3.TabIndex = 13;
            this.label3.Text = "Jugador";
            // 
            // c1f1
            // 
            this.c1f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1f1.Location = new System.Drawing.Point(21, 25);
            this.c1f1.Name = "c1f1";
            this.c1f1.ReadOnly = true;
            this.c1f1.Size = new System.Drawing.Size(80, 128);
            this.c1f1.TabIndex = 14;
            // 
            // c2f1
            // 
            this.c2f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2f1.Location = new System.Drawing.Point(107, 25);
            this.c2f1.Name = "c2f1";
            this.c2f1.ReadOnly = true;
            this.c2f1.Size = new System.Drawing.Size(80, 128);
            this.c2f1.TabIndex = 15;
            // 
            // c3f1
            // 
            this.c3f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3f1.Location = new System.Drawing.Point(193, 25);
            this.c3f1.Name = "c3f1";
            this.c3f1.ReadOnly = true;
            this.c3f1.Size = new System.Drawing.Size(80, 128);
            this.c3f1.TabIndex = 16;
            // 
            // c1f2
            // 
            this.c1f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1f2.Location = new System.Drawing.Point(21, 159);
            this.c1f2.Name = "c1f2";
            this.c1f2.ReadOnly = true;
            this.c1f2.Size = new System.Drawing.Size(80, 128);
            this.c1f2.TabIndex = 17;
            // 
            // c2f2
            // 
            this.c2f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2f2.Location = new System.Drawing.Point(107, 159);
            this.c2f2.Name = "c2f2";
            this.c2f2.ReadOnly = true;
            this.c2f2.Size = new System.Drawing.Size(80, 128);
            this.c2f2.TabIndex = 18;
            // 
            // c3f2
            // 
            this.c3f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3f2.Location = new System.Drawing.Point(193, 159);
            this.c3f2.Name = "c3f2";
            this.c3f2.ReadOnly = true;
            this.c3f2.Size = new System.Drawing.Size(80, 128);
            this.c3f2.TabIndex = 19;
            // 
            // c1f3
            // 
            this.c1f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c1f3.Location = new System.Drawing.Point(21, 293);
            this.c1f3.Name = "c1f3";
            this.c1f3.ReadOnly = true;
            this.c1f3.Size = new System.Drawing.Size(80, 128);
            this.c1f3.TabIndex = 20;
            // 
            // c2f3
            // 
            this.c2f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c2f3.Location = new System.Drawing.Point(107, 293);
            this.c2f3.Name = "c2f3";
            this.c2f3.ReadOnly = true;
            this.c2f3.Size = new System.Drawing.Size(80, 128);
            this.c2f3.TabIndex = 21;
            // 
            // c3f3
            // 
            this.c3f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.c3f3.Location = new System.Drawing.Point(193, 293);
            this.c3f3.Name = "c3f3";
            this.c3f3.ReadOnly = true;
            this.c3f3.Size = new System.Drawing.Size(80, 128);
            this.c3f3.TabIndex = 22;
            // 
            // IAc3f3
            // 
            this.IAc3f3.Enabled = false;
            this.IAc3f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc3f3.Location = new System.Drawing.Point(722, 311);
            this.IAc3f3.Name = "IAc3f3";
            this.IAc3f3.ReadOnly = true;
            this.IAc3f3.Size = new System.Drawing.Size(80, 128);
            this.IAc3f3.TabIndex = 31;
            // 
            // IAc2f3
            // 
            this.IAc2f3.Enabled = false;
            this.IAc2f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc2f3.Location = new System.Drawing.Point(636, 311);
            this.IAc2f3.Name = "IAc2f3";
            this.IAc2f3.ReadOnly = true;
            this.IAc2f3.Size = new System.Drawing.Size(80, 128);
            this.IAc2f3.TabIndex = 30;
            // 
            // IAc1f3
            // 
            this.IAc1f3.Enabled = false;
            this.IAc1f3.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc1f3.Location = new System.Drawing.Point(550, 311);
            this.IAc1f3.Name = "IAc1f3";
            this.IAc1f3.ReadOnly = true;
            this.IAc1f3.Size = new System.Drawing.Size(80, 128);
            this.IAc1f3.TabIndex = 29;
            // 
            // IAc3f2
            // 
            this.IAc3f2.Enabled = false;
            this.IAc3f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc3f2.Location = new System.Drawing.Point(722, 177);
            this.IAc3f2.Name = "IAc3f2";
            this.IAc3f2.ReadOnly = true;
            this.IAc3f2.Size = new System.Drawing.Size(80, 128);
            this.IAc3f2.TabIndex = 28;
            // 
            // IAc2f2
            // 
            this.IAc2f2.Enabled = false;
            this.IAc2f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc2f2.Location = new System.Drawing.Point(636, 177);
            this.IAc2f2.Name = "IAc2f2";
            this.IAc2f2.ReadOnly = true;
            this.IAc2f2.Size = new System.Drawing.Size(80, 128);
            this.IAc2f2.TabIndex = 27;
            // 
            // IAc1f2
            // 
            this.IAc1f2.Enabled = false;
            this.IAc1f2.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc1f2.Location = new System.Drawing.Point(550, 177);
            this.IAc1f2.Name = "IAc1f2";
            this.IAc1f2.ReadOnly = true;
            this.IAc1f2.Size = new System.Drawing.Size(80, 128);
            this.IAc1f2.TabIndex = 26;
            // 
            // IAc3f1
            // 
            this.IAc3f1.Enabled = false;
            this.IAc3f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc3f1.Location = new System.Drawing.Point(722, 43);
            this.IAc3f1.Name = "IAc3f1";
            this.IAc3f1.ReadOnly = true;
            this.IAc3f1.Size = new System.Drawing.Size(80, 128);
            this.IAc3f1.TabIndex = 25;
            // 
            // IAc2f1
            // 
            this.IAc2f1.Enabled = false;
            this.IAc2f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc2f1.Location = new System.Drawing.Point(636, 43);
            this.IAc2f1.Name = "IAc2f1";
            this.IAc2f1.ReadOnly = true;
            this.IAc2f1.Size = new System.Drawing.Size(80, 128);
            this.IAc2f1.TabIndex = 24;
            // 
            // IAc1f1
            // 
            this.IAc1f1.Enabled = false;
            this.IAc1f1.Font = new System.Drawing.Font("Microsoft Sans Serif", 80F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.IAc1f1.Location = new System.Drawing.Point(550, 43);
            this.IAc1f1.Name = "IAc1f1";
            this.IAc1f1.ReadOnly = true;
            this.IAc1f1.Size = new System.Drawing.Size(80, 128);
            this.IAc1f1.TabIndex = 23;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(574, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(205, 25);
            this.label1.TabIndex = 32;
            this.label1.Text = "Pensamiento MiniMax";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(931, 450);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.IAc3f3);
            this.Controls.Add(this.IAc2f3);
            this.Controls.Add(this.IAc1f3);
            this.Controls.Add(this.IAc3f2);
            this.Controls.Add(this.IAc2f2);
            this.Controls.Add(this.IAc1f2);
            this.Controls.Add(this.IAc3f1);
            this.Controls.Add(this.IAc2f1);
            this.Controls.Add(this.IAc1f1);
            this.Controls.Add(this.c3f3);
            this.Controls.Add(this.c2f3);
            this.Controls.Add(this.c1f3);
            this.Controls.Add(this.c3f2);
            this.Controls.Add(this.c2f2);
            this.Controls.Add(this.c1f2);
            this.Controls.Add(this.c3f1);
            this.Controls.Add(this.c2f1);
            this.Controls.Add(this.c1f1);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.textBox11);
            this.Controls.Add(this.textBox10);
            this.Controls.Add(this.txtTurno);
            this.Name = "Form1";
            this.Text = "Form1";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label txtTurno;
        private System.Windows.Forms.TextBox textBox10;
        private System.Windows.Forms.TextBox textBox11;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private Controls.Casillero c1f1;
        private Controls.Casillero c2f1;
        private Controls.Casillero c3f1;
        private Controls.Casillero c1f2;
        private Controls.Casillero c2f2;
        private Controls.Casillero c3f2;
        private Controls.Casillero c1f3;
        private Controls.Casillero c2f3;
        private Controls.Casillero c3f3;
        private Controls.Casillero IAc3f3;
        private Controls.Casillero IAc2f3;
        private Controls.Casillero IAc1f3;
        private Controls.Casillero IAc3f2;
        private Controls.Casillero IAc2f2;
        private Controls.Casillero IAc1f2;
        private Controls.Casillero IAc3f1;
        private Controls.Casillero IAc2f1;
        private Controls.Casillero IAc1f1;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Timer timer1;
    }
}

