﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Controls;

namespace IA_TaTeTi
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }
        public List<Casillero> Casilleros = new List<Casillero>();

        #region Declaraciones IA2
        public List<Casillero> CasillerosF1 = new List<Casillero>();
        public List<Casillero> CasillerosF2 = new List<Casillero>();
        public List<Casillero> CasillerosF3 = new List<Casillero>();
        public List<Casillero> CasillerosC1 = new List<Casillero>();
        public List<Casillero> CasillerosC2 = new List<Casillero>();
        public List<Casillero> CasillerosC3 = new List<Casillero>();
        public List<Casillero> CasillerosD1 = new List<Casillero>();
        public List<Casillero> CasillerosD2 = new List<Casillero>();
        #endregion
        Random ran = new Random();

        private void Form1_Load(object sender, EventArgs e)
        {
            #region AsignacionesIA1
            c1f1.SeJugo += JugoJugador;
            c1f1.columna = 1; c1f1.fila = 1;
            c1f2.SeJugo += JugoJugador;
            c1f2.columna = 1; c1f2.fila = 2;
            c1f3.SeJugo += JugoJugador;
            c1f3.columna = 1; c1f3.fila = 3;
            c2f1.SeJugo += JugoJugador;
            c2f1.columna = 2; c2f1.fila = 1;
            c2f2.SeJugo += JugoJugador;
            c2f2.columna = 2; c2f2.fila = 2;
            c2f3.SeJugo += JugoJugador;
            c2f3.columna = 2; c2f3.fila = 3;
            c3f1.SeJugo += JugoJugador;
            c3f1.columna = 3; c3f1.fila = 1;
            c3f2.SeJugo += JugoJugador;
            c3f2.columna = 3; c3f2.fila = 2;
            c3f3.SeJugo += JugoJugador;
            c3f3.columna = 3; c3f3.fila = 3;
            Casilleros.Add(c1f1);
            Casilleros.Add(c1f2);
            Casilleros.Add(c1f3);
            Casilleros.Add(c2f1);
            Casilleros.Add(c2f2);
            Casilleros.Add(c2f3);
            Casilleros.Add(c3f1);
            Casilleros.Add(c3f2);
            Casilleros.Add(c3f3);
            #endregion

            #region AsignacionesIA2
            CasillerosC1.Add(c1f1);
            CasillerosC1.Add(c1f2);
            CasillerosC1.Add(c1f3);

            CasillerosC2.Add(c2f1);
            CasillerosC2.Add(c2f2);
            CasillerosC2.Add(c2f3);

            CasillerosC3.Add(c3f1);
            CasillerosC3.Add(c3f2);
            CasillerosC3.Add(c3f3);

            CasillerosF1.Add(c1f1);
            CasillerosF1.Add(c2f1);
            CasillerosF1.Add(c3f1);

            CasillerosF2.Add(c1f2);
            CasillerosF2.Add(c2f2);
            CasillerosF2.Add(c3f2);

            CasillerosF3.Add(c1f3);
            CasillerosF3.Add(c2f3);
            CasillerosF3.Add(c3f3);

            CasillerosD1.Add(c1f1);
            CasillerosD1.Add(c2f2);
            CasillerosD1.Add(c3f3);

            CasillerosD2.Add(c3f1);
            CasillerosD2.Add(c2f2);
            CasillerosD2.Add(c1f3);
            #endregion

            #region AsignacionesIA3
            CasillerosIA.Add(IAc1f1);
            CasillerosIA.Add(IAc1f2);
            CasillerosIA.Add(IAc1f3);
            CasillerosIA.Add(IAc2f1);
            CasillerosIA.Add(IAc2f2);
            CasillerosIA.Add(IAc2f3);
            CasillerosIA.Add(IAc3f1);
            CasillerosIA.Add(IAc3f2);
            CasillerosIA.Add(IAc3f3);

            IAc1f1.columna = 1; IAc1f1.fila = 1;
            IAc1f2.columna = 1; IAc1f2.fila = 2;
            IAc1f3.columna = 1; IAc1f3.fila = 3;
            IAc2f1.columna = 2; IAc2f1.fila = 1;
            IAc2f2.columna = 2; IAc2f2.fila = 2;
            IAc2f3.columna = 2; IAc2f3.fila = 3;
            IAc3f1.columna = 3; IAc3f1.fila = 1;
            IAc3f2.columna = 3; IAc3f2.fila = 2;
            IAc3f3.columna = 3; IAc3f3.fila = 3;
            #endregion
        }

        public void JugoJugador()
        {
            foreach (Casillero c in Casilleros)
            {
                c.Enabled = false;
            }
            if(ChequearVictoria() == false)
            {
                
                //JuegoIA1();
                JuegoIA2();              
            }
        }

        public void JuegoIA1()
        {
            int h = ran.Next(0,9);
            if(Casilleros[h].estado == 0)
            {
                Casilleros[h].Text = "X";
                if(ChequearVictoria() == false)
                {
                    foreach (Casillero c in Casilleros)
                    {
                        if (c.Text == "")
                        {
                            c.Enabled = true;
                        }
                    }
                }
            }
            else
            {
                JuegoIA1();
                return;
            }
        }

        public void JuegoIA2()
        {
            if(ChequearSiJugadorVaAGanar() != null)
            {
                ChequearSiJugadorVaAGanar().Text = "X";
                if (ChequearVictoria() == false)
                {
                    foreach (Casillero c in Casilleros)
                    {
                        if (c.Text == "")
                        {
                            c.Enabled = true;
                        }
                    }
                }
            }
            else
            {
                int h = ran.Next(0, 9);
                if (Casilleros[h].estado == 0)
                {
                    Casilleros[h].Text = "X";
                    if (ChequearVictoria() == false)
                    {
                        foreach (Casillero c in Casilleros)
                        {
                            if (c.Text == "")
                            {
                                c.Enabled = true;
                            }
                        }
                    }
                }
                else
                {
                    JuegoIA2();
                    return;
                }
            }
        }


        public bool ChequearVictoria()
        {
            int chequearEmpate = 0;
            if (c1f1.estado + c1f2.estado + c1f3.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c1f1.estado + c1f2.estado + c1f3.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }

            if (c1f1.estado + c2f1.estado + c3f1.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c1f1.estado + c2f1.estado + c3f1.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }

            if (c1f1.estado + c2f2.estado + c3f3.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c1f1.estado + c2f2.estado + c3f3.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }

            if (c1f2.estado + c2f2.estado + c3f2.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c1f2.estado + c2f2.estado + c3f2.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }

            if (c1f3.estado + c2f3.estado + c3f3.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c1f3.estado + c2f3.estado + c3f3.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }

            if (c2f1.estado + c2f2.estado + c2f3.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c2f1.estado + c2f2.estado + c2f3.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }

            if (c3f1.estado + c3f2.estado + c3f3.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c3f1.estado + c3f2.estado + c3f3.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }

            if (c3f1.estado + c2f2.estado + c1f3.estado == 3)
            {
                MessageBox.Show("Gano el Jugador");
                return true;
            }
            if (c3f1.estado + c2f2.estado + c1f3.estado == -3)
            {
                MessageBox.Show("Gano la IA");
                return true;
            }
            foreach(Casillero c in Casilleros)
            {
                if(c.estado != 0)
                {
                    chequearEmpate++;
                }
            }
            if(chequearEmpate == 9)
            {
                MessageBox.Show("Empate");
                return true;
            }
            else
            {
                return false;
            }
        }

        public Casillero ChequearSiJugadorVaAGanar()
        {
            int control = 0;

            //Columna 1
            foreach(Casillero c in CasillerosC1)
            {
                control = control + c.estado;
            }
            if(control == 2)
            {
                foreach (Casillero c in CasillerosC1)
                {
                    if(c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            //Columna 2
            foreach (Casillero c in CasillerosC2)
            {
                control = control + c.estado;
            }
            if (control == 2)
            {
                foreach (Casillero c in CasillerosC2)
                {
                    if (c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            //Columna 3
            foreach (Casillero c in CasillerosC3)
            {
                control = control + c.estado;
            }
            if (control == 2)
            {
                foreach (Casillero c in CasillerosC3)
                {
                    if (c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            //Fila 1
            foreach (Casillero c in CasillerosF1)
            {
                control = control + c.estado;
            }
            if (control == 2)
            {
                foreach (Casillero c in CasillerosF1)
                {
                    if (c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            //Fila 2
            foreach (Casillero c in CasillerosF2)
            {
                control = control + c.estado;
            }
            if (control == 2)
            {
                foreach (Casillero c in CasillerosF2)
                {
                    if (c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            //Fila 3
            foreach (Casillero c in CasillerosF3)
            {
                control = control + c.estado;
            }
            if (control == 2)
            {
                foreach (Casillero c in CasillerosF3)
                {
                    if (c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            //Diagonal 1
            foreach (Casillero c in CasillerosD1)
            {
                control = control + c.estado;
            }
            if (control == 2)
            {
                foreach (Casillero c in CasillerosD1)
                {
                    if (c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            //Diagonal 2
            foreach (Casillero c in CasillerosD2)
            {
                control = control + c.estado;
            }
            if (control == 2)
            {
                foreach (Casillero c in CasillerosD2)
                {
                    if (c.estado == 0)
                    {
                        return c;
                    }
                }
            }
            else { control = 0; }

            return null;
        }

    }
    // ---------------------------> Algoritmo MiniMax <---------------------------

    //    public List<Casillero> CasillerosIA = new List<Casillero>();
    //    public void JuegoAI3()
    //    {
    //        Casillero casilleroAJugar = CrearEscenariosPosibles();
    //        if(casilleroAJugar == null)
    //        {
    //            foreach (Casillero c in Casilleros)
    //            {
    //                if(c.estado == 0)
    //                {
    //                    c.Text = "X";
    //                    break;
    //                }
    //            }
    //        }
    //        if(casilleroAJugar != null)
    //        {
    //            foreach (Casillero c in Casilleros)
    //            {
    //                if (c.fila == casilleroAJugar.fila && c.columna == casilleroAJugar.columna)
    //                {
    //                    c.Text = "X";
    //                }
    //            }
    //        }           

    //        if (ChequearVictoria() == false)
    //        {
    //            foreach (Casillero c in Casilleros)
    //            {
    //                if (c.Text == "")
    //                {
    //                    c.Enabled = true;
    //                }
    //            }
    //        }
    //    }

    //    public int ChequearVictoriaMiniMax()
    //    {
    //        int chequearEmpate = 0;
    //        if (IAc1f1.estado + IAc1f2.estado + IAc1f3.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc1f1.estado + IAc1f2.estado + IAc1f3.estado == -3)
    //        {
    //            return 1;
    //        }

    //        if (IAc1f1.estado + IAc2f1.estado + IAc3f1.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc1f1.estado + IAc2f1.estado + IAc3f1.estado == -3)
    //        {
    //            return 1;
    //        }

    //        if (IAc1f1.estado + IAc2f2.estado + IAc3f3.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc1f1.estado + IAc2f2.estado + IAc3f3.estado == -3)
    //        {
    //            return 1;
    //        }

    //        if (IAc1f2.estado + IAc2f2.estado + IAc3f2.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc1f2.estado + IAc2f2.estado + IAc3f2.estado == -3)
    //        {
    //            return 1;
    //        }

    //        if (IAc1f3.estado + IAc2f3.estado + IAc3f3.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc1f3.estado + IAc2f3.estado + IAc3f3.estado == -3)
    //        {
    //            return 1;
    //        }

    //        if (IAc2f1.estado + IAc2f2.estado + IAc2f3.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc2f1.estado + IAc2f2.estado + IAc2f3.estado == -3)
    //        {
    //            return 1;
    //        }

    //        if (IAc3f1.estado + IAc3f2.estado + IAc3f3.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc3f1.estado + IAc3f2.estado + IAc3f3.estado == -3)
    //        {
    //            return 1;
    //        }

    //        if (IAc3f1.estado + IAc2f2.estado + IAc1f3.estado == 3)
    //        {
    //            return 0;
    //        }
    //        if (IAc3f1.estado + IAc2f2.estado + IAc1f3.estado == -3)
    //        {
    //            return 1;
    //        }
    //        foreach (Casillero c in CasillerosIA)
    //        {
    //            if (c.estado != 0)
    //            {
    //                chequearEmpate++;
    //            }
    //        }
    //        if (chequearEmpate == 9)
    //        {
    //            //es emapte
    //            return -1;
    //        }
    //        else
    //        {
    //            return -2;
    //        }
    //    }

    //    public Casillero CrearEscenariosPosibles()
    //    {
    //        for(int i = 0;i < 9; i++)
    //        {
    //            CasillerosIA[i].estado = Casilleros[i].estado;
    //            CasillerosIA[i].Text = Casilleros[i].Text;
    //        }
    //        foreach(Casillero c in CasillerosIA)
    //        {
    //            if(c.estado != 0)
    //            {
    //                c.testedO = true;
    //                c.testedX = true;
    //            }
    //        }
    //        return ProbarUnaRama();

    //    }

    //    public Casillero ProbarUnaRama()
    //    {
    //        Casillero casilleroAJugar = null;
    //        bool cambiarDeRama = false;

    //        foreach (Casillero c in CasillerosIA)
    //        {
    //            for (int i = 0; i < 9; i++)
    //            {
    //                CasillerosIA[i].estado = Casilleros[i].estado;
    //                CasillerosIA[i].Text = Casilleros[i].Text;
    //                CasillerosIA[i].testedO = Casilleros[i].testedO;
    //                CasillerosIA[i].testedX = Casilleros[i].testedX;
    //            }
    //            foreach (Casillero c1 in CasillerosIA)
    //            {
    //                if (c1.estado != 0)
    //                {
    //                    c1.testedO = true;
    //                    c1.testedX = true;
    //                }
    //            }

    //            if (c.estado == 0 && c.fueInicioDeRama == false)
    //            {
    //                int X = 0;
    //                int O = 0;
    //                c.estado = -1;
    //                c.Text = "X";
    //                c.testedX = true;
    //                c.testedO = true;
    //                c.fueInicioDeRama = true;
    //                while (ChequearVictoriaMiniMax() == -2 && cambiarDeRama == false)
    //                {
    //                    JugarUnCasilleroJugador();
    //                    JugarUnCasilleroIA();
    //                    if(ChequearVictoriaMiniMax() == 1)
    //                    {
    //                        casilleroAJugar = c;
    //                    }
    //                    foreach(Casillero casillero in CasillerosIA)
    //                    {
    //                        if(casillero.testedX == true)
    //                        {
    //                            X++;
    //                        }
    //                        if (casillero.testedO == true)
    //                        {
    //                            O++;
    //                        }
    //                    }
    //                    if( X != 9)
    //                    {
    //                        X = 0;
    //                    }
    //                    if (O != 9)
    //                    {
    //                        O = 0;
    //                    }
    //                    if (X == 9 && O == 9)
    //                    {
    //                        cambiarDeRama = true;
    //                    }

    //                }
    //                if(ChequearVictoriaMiniMax() == 1)
    //                {
    //                    casilleroAJugar = null;
    //                    casilleroAJugar = c;
    //                    return casilleroAJugar;
    //                }
    //                if (ChequearVictoriaMiniMax() == -1 && casilleroAJugar == null)
    //                {
    //                    casilleroAJugar = c;
    //                }
    //                c.testedO = false;
    //                c.testedX = false;
    //            }
    //        }
    //        return casilleroAJugar;
    //    }

    //    public Casillero Prueba(Casillero c)
    //    {
    //        Casillero casilleroAJugar = null;
    //        bool cambiarDeRama = false;
    //        int i = 0;
    //        while (ChequearVictoriaMiniMax() == -2 && cambiarDeRama == false)
    //        {
    //            JugarUnCasilleroJugador();
    //            JugarUnCasilleroIA();
    //            if (ChequearVictoriaMiniMax() == 1)
    //            {
    //                casilleroAJugar = c;
    //            }
    //            foreach (Casillero casillero in CasillerosIA)
    //            {
    //                if (casillero.testedO == true && casillero.testedX == true)
    //                {
    //                    i++;
    //                }
    //            }
    //            if (i != 9)
    //            {
    //                i = 0;
    //            }
    //            if (i == 9)
    //            {
    //                cambiarDeRama = true;
    //            }
    //        }
    //        if (ChequearVictoriaMiniMax() == 1)
    //        {
    //            casilleroAJugar = null;
    //            casilleroAJugar = c;
    //            return casilleroAJugar;
    //        }
    //        if (ChequearVictoriaMiniMax() == -1 && casilleroAJugar == null)
    //        {
    //            casilleroAJugar = c;
    //        }
    //        c.testedO = false;
    //        return casilleroAJugar;
    //    }


    //    public void JugarUnCasilleroIA()
    //    {
    //        foreach(Casillero c in CasillerosIA)
    //        {
    //            if(c.estado == 0 && c.testedX == false)
    //            {
    //                c.estado = -1;
    //                c.Text = "X";
    //                c.testedX = true;
    //                break;
    //            }
    //        }          
    //    }

    //    public void JugarUnCasilleroJugador()
    //    {
    //        foreach (Casillero c in CasillerosIA)
    //        {
    //            if (c.estado == 0 && c.testedO == false)
    //            {
    //                c.estado = 1;
    //                c.Text = "O";
    //                c.testedO = true;
    //                break;
    //            }
    //        }
    //    }
    //}
}
