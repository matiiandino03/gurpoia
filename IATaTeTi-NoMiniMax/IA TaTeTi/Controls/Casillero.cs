﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Controls
{
    public class Casillero : TextBox
    {
        public int estado = 0;
        public delegate void JugadoJugo();
        public event JugadoJugo SeJugo;

        public int columna;
        public int fila;

        public bool testedX = false;
        public bool testedO = false;
        public bool fueInicioDeRama = false;


        protected override void InitLayout()
        {
            base.InitLayout();
            Click += ClickJuego;
            TextChanged += CambioTexto;
        }

        public void ClickJuego(object sender, EventArgs e)
        {
            if(this.Enabled == true)
            {
                Text = "O";
                estado = 1;
                SeJugo.Invoke();               
            }
            else
            {
                MessageBox.Show("No es tu turno");
            }
        }

        public void CambioTexto(object sender, EventArgs e)
        {
            if(Text == "O")
            {
                estado = 1;
            }
            if(Text == "X")
            {
                estado = -1;
            }
        }
       
    }
}
